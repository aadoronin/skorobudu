import Vue from 'vue'
import Router from 'vue-router'
import Login from './views/Login'
import Places from './views/Places'
import List from './views/List'
import Cafe from './views/cafe/List'
import Pay from './views/cafe/Pay'
import LastOrders from './views/LastOrders'
import Settings from './views/Settings'
import OnBoarding from './views/onboarding'
import {LocalStorage} from 'vuetify'

Vue.use(Router)

export default new Router({
    routes: [
        {
            path: '/login',
            name: 'login',
            component: Login
        },
        {
            path: '/places',
            name: 'places',
            component: Places
        },
        {
            path: '/list',
            name: 'list',
            component: List
        },
        {
            path: '/cafe-item/:id',
            name: 'cafeItem',
            component: Cafe
        },
        {
            path: '/pay',
            name: 'pay',
            component: Pay
        },
        {
            path: '/lastOrders',
            name: 'lastOrders',
            component: LastOrders
        },
        {
            path: '/settings',
            name: 'settings',
            component: Settings
        },
        {
            path: '/onboarding',
            name: 'onboarding',
            component: OnBoarding
        }
    ]
})
