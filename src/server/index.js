/* eslint-disable */

const origin = process.env.VUE_APP_API_PATH

export default {
  authLogin: {
    url: `/client/register`,
    method: 'POST',
    headers: {
      "Content-Type": "application/x-www-form-urlencoded"
    },
    data: {}
  },
  getCafeList: {
    url: `/cafe/list`,
    method: 'GET',
    headers: {
      "Content-Type": "application/x-www-form-urlencoded",
      "authorization": ''
    }
  },
  getMenuList: {
    url: `/cafe/:id/menu`,
    method: 'GET',
    headers: {
      "Content-Type": "application/x-www-form-urlencoded",
      "authorization": ''
    }
  },
  getOrderById: {
    url: `/orders/:id`,
    method: 'GET',
    headers: {
      "Content-Type": "application/x-www-form-urlencoded",
      "authorization": ''
    }
  },
  getLastOrders: {
    url: `/orders?kind=latest`,
    method: 'GET',
    headers: {
      "Content-Type": "application/x-www-form-urlencoded",
      "authorization": ''
    }
  },
  getDiscount: {
    url: `/discount?price=0`,
    method: 'GET',
    headers: {
      "Content-Type": "application/x-www-form-urlencoded",
      "authorization": ''
    }
  },
  getPaymentLast: {
    url: `/payments/last?order_id=:id`,
    method: 'GET',
    headers: {
      "Content-Type": "application/x-www-form-urlencoded",
      "authorization": ''
    }
  },
  postOrders: {
    url: `/orders`,
    method: 'POST',
    headers: {
      "Content-Type": "application/x-www-form-urlencoded"
    },
    data: {}
  },
  postPayments: {
    url: `/payments`,
    method: 'POST',
    headers: {
      "Content-Type": "application/x-www-form-urlencoded"
    },
    data: {}
  },
  postOrderStatus: {
    url: `/orders/:id/statuses`,
    method: 'POST',
    headers: {
      "Content-Type": "application/x-www-form-urlencoded"
    },
    data: {}
  },
  postFirebaseToken: {
    url: `/client/token`,
    method: 'POST',
    headers: {
      "Content-Type": "application/x-www-form-urlencoded"
    },
    data: {}
  },
}
