/* eslint-disable */

import axios from 'axios'
import {AUTH_REQUEST, AUTH_ERROR, AUTH_SUCCESS, AUTH_LOGOUT} from '../actions/auth'
import {SERVER_GET_DISCOUNT, SERVER_SEND_FIREBASE_TOKEN} from '../actions/server'
import server from '../../server/index'
import Vue from 'vue'

const state = {
    token: sessionStorage.getItem('user-token') || '',
    status: '',
    hasLoadedOnce: false,
    pending: false
}

const getters = {
    isAuthenticated: state => !!state.token,
    authStatus: state => state.status,
    authPending: state => state.pending
}

const actions = {
    [AUTH_REQUEST]: ({commit, dispatch}, data) => {
        return new Promise((resolve, reject) => {
            commit(AUTH_REQUEST)
            const authLogin = server.authLogin
            Object.assign(authLogin.data, data)

            axios(authLogin)
                .then(resp => {
                    console.log('login response', resp)
                    return !resp.data.Error
                        ? Promise.resolve(resp)
                        : Promise.reject(resp)
                })
                .then(resp => {
                    localStorage.setItem('user-token', resp.data.Token)
                    localStorage.setItem('user-login', data.login)
                    dispatch(SERVER_GET_DISCOUNT)

                    getFirebaseToken()
                        .then(firebaseToken => {
                            dispatch(SERVER_SEND_FIREBASE_TOKEN, {
                                'Login': data.login,
                                'Token': resp.data.Token,
                                'FbToken': firebaseToken
                            })
                        })


                    commit(AUTH_SUCCESS, resp.data)

                    resolve(resp.data)
                })
                .catch(resp => {
                    console.log('auth/login err', resp)

                    commit(AUTH_ERROR, resp)
                    //localStorage.removeItem('user-token')
                    reject(resp)
                })
        })
    },
    [AUTH_LOGOUT]: ({commit, dispatch}) => {
        return new Promise((resolve, reject) => {
            commit(AUTH_LOGOUT)
            localStorage.removeItem('user-token')
            localStorage.removeItem('user-login')
            localStorage.removeItem('profile')
            return resolve()
        })
    }
}

const mutations = {
    [AUTH_REQUEST]: (state) => {
        state.status = 'loading'
        state.pending = true
    },
    [AUTH_SUCCESS]: (state, resp) => {
        state.status = 'success'
        state.token = resp.Token
        state.hasLoadedOnce = true
        state.pending = false
    },
    [AUTH_ERROR]: (state) => {
        state.status = 'error'
        state.hasLoadedOnce = true
        state.pending = false
    },
    [AUTH_LOGOUT]: (state) => {
        state.token = ''
    }
}

export default {
    state,
    getters,
    actions,
    mutations
}

function getFirebaseToken() {
    return new Promise((resolve, reject) => {
        if (!window.FirebasePlugin) {
            console.log("getFirebaseToken - 1")
            resolve()
        } else {
            console.log("getFirebaseToken - 2")
            window.FirebasePlugin.getToken(function (token) {
                console.log('firebase ' + token)
                resolve(token)
            }, function (error) {
                console.log('firebase ' + error)
                reject(error)
            })
        }
    })
}
