/* eslint-disable */

import axios from 'axios'
import {
    SERVER_CAFE_LIST,
    SERVER_MENU_LIST,
    SERVER_SEND_PAYMENT_TOKEN,
    SERVER_SEND_ORDER,
    SERVER_SEND_FIREBASE_TOKEN,
    SERVER_GET_ORDER_BY_ID,
    SERVER_GET_PAYMENT,
    SERVER_POST_ORDER,
    SERVER_LAST_ORDERS,
    SERVER_GET_DISCOUNT,
    SET_BASKET
} from '../actions/server'
import server from '../../server/index'
import {AUTH_ERROR, AUTH_SUCCESS} from '../actions/auth'

const state = {
    cafeList: [],
    lastOrder: {},
    lastGetPayment: {},
    basket: {
        cafeId: '',
        items: []
    },
    discount: {
        Text: 'Скидка на 1-й заказ – 50 р., на 2-ой – 100 р.'
    }
}

const getters = {}

const actions = {
    [SERVER_CAFE_LIST]: ({commit, dispatch}) => {
        return new Promise((resolve, reject) => {

            const getCafeList = server.getCafeList
            getCafeList.headers.authorization = 'Token ' + localStorage.getItem('user-token')
            console.log('getCafeList', getCafeList)

            axios(getCafeList)
                .then(resp => {
                    console.log('cafe list response', resp)
                    return !resp.data.Error
                        ? Promise.resolve(resp)
                        : Promise.reject(resp)
                })
                .then(resp => {
                    const data = resp.data

                    commit(SERVER_CAFE_LIST, data)
                    resolve(data)
                })
                .catch(resp => {
                    console.log('auth/login err', resp)

                    commit(AUTH_ERROR, resp)
                    localStorage.removeItem('user-token')
                    reject(resp)
                })
        })
    },
    [SERVER_MENU_LIST]: ({commit, dispatch}, cafeId) => {
        return new Promise((resolve, reject) => {

            const getMenuList = server.getMenuList
            getMenuList.headers.authorization = 'Token ' + localStorage.getItem('user-token')
            getMenuList.url = `/cafe/${cafeId}/menu`

            axios(getMenuList)
                .then(resp => {
                    console.log('menu list response', resp)
                    return !resp.data.Error
                        ? Promise.resolve(resp)
                        : Promise.reject(resp)
                })
                .then(resp => {
                    const data = resp.data
                    resolve(data)
                })
                .catch(resp => {
                    console.log('menu list err', resp)

                    reject(resp)
                })
        })
    },
    [SERVER_SEND_PAYMENT_TOKEN]: ({commit, dispatch}, query) => {
        return new Promise((resolve, reject) => {

            const postPayments = server.postPayments
            postPayments.headers.authorization = 'Token ' + localStorage.getItem('user-token')
            Object.assign(postPayments.data, query)
            console.log('send post payments', postPayments)

            axios(postPayments)
                .then(resp => {
                    console.log('menu list response', resp)
                    return !resp.data.Error
                        ? Promise.resolve(resp)
                        : Promise.reject(resp)
                })
                .then(resp => {
                    const data = resp.data
                    resolve(data)
                })
                .catch(resp => {
                    console.log('menu list err', resp)

                    reject(resp)
                })
        })
    },
    [SERVER_GET_PAYMENT]: ({commit, dispatch}, orderId) => {
        return new Promise((resolve, reject) => {

            const getPaymentLast = server.getPaymentLast
            getPaymentLast.headers.authorization = 'Token ' + localStorage.getItem('user-token')
            getPaymentLast.url = `/payments/last?order_id=${orderId}`
            console.log('get payment', getPaymentLast)

            axios(getPaymentLast)
                .then(resp => {
                    console.log('get payment response', resp)
                    return !resp.data.Error
                        ? Promise.resolve(resp)
                        : Promise.reject(resp)
                })
                .then(resp => {
                    const data = resp.data
                    resolve(data)
                })
                .catch(resp => {
                    console.log('menu list err', resp)

                    reject(resp)
                })
        })
    },
    [SERVER_SEND_ORDER]: ({commit, dispatch}, order) => {
        return new Promise((resolve, reject) => {

            const postOrders = server.postOrders
            postOrders.headers.authorization = 'Token ' + localStorage.getItem('user-token')
            Object.assign(postOrders.data, order)
            console.log('send post order', order)

            axios(postOrders)
                .then(resp => {
                    console.log('post order response', resp)
                    return !resp.data.Error
                        ? Promise.resolve(resp)
                        : Promise.reject(resp)
                })
                .then(resp => {

                    console.log("resp.order.TotalPrice1")

                    console.log(resp, 'resp.order.TotalPrice')

                    const lastOrder = {
                        orderId: resp.data.ReturnedId,
                        total: order.TotalPrice,
                    }

                    commit(SERVER_SEND_ORDER, lastOrder)
                    resolve(resp.data)
                })
                .catch(resp => {
                    console.log('menu list err', resp)

                    reject(resp)
                })
        })
    },
    [SERVER_SEND_FIREBASE_TOKEN]: ({commit, dispatch}, query) => {
        return new Promise((resolve, reject) => {

            const postFirebaseToken = server.postFirebaseToken
            postFirebaseToken.headers.authorization = 'Token ' + localStorage.getItem('user-token')
            Object.assign(postFirebaseToken.data, query)
            console.log('send post firebase token', query)

            axios(postFirebaseToken)
                .then(resp => {
                    console.log('post firebase token response', resp)
                    return !resp.data.Error
                        ? Promise.resolve(resp)
                        : Promise.reject(resp)
                })
                .then(resp => {

                    const lastOrder = {
                        orderId: resp.data.ReturnedId,
                        total: order.TotalPrice,
                    }

                    onNotificationOpen()

                    commit(SERVER_SEND_ORDER, lastOrder)
                    resolve(resp.data)
                })
                .catch(resp => {
                    console.log('SERVER_SEND_ORDER err', resp)

                    reject(resp)
                })
        })
    },
    [SERVER_GET_ORDER_BY_ID]: ({commit, dispatch}, orderId) => {
        return new Promise((resolve, reject) => {

            const getOrderById = server.getOrderById
            getOrderById.headers.authorization = 'Token ' + localStorage.getItem('user-token')
            getOrderById.url = `/orders/${orderId}`

            axios(getOrderById)
                .then(resp => {
                    console.log('SERVER_GET_ORDER_BY_ID response', resp)
                    return !resp.data.Error
                        ? Promise.resolve(resp)
                        : Promise.reject(resp)
                })
                .then(resp => {
                    const data = resp.data
                    commit(SERVER_GET_ORDER_BY_ID, data)
                    resolve(data)
                })
                .catch(resp => {
                    console.log('menu list err', resp)

                    reject(resp)
                })
        })
    },
    [SERVER_POST_ORDER]: ({commit, dispatch}, query) => {
        return new Promise((resolve, reject) => {

            const postOrderStatus = server.postOrderStatus
            postOrderStatus.headers.authorization = 'Token ' + localStorage.getItem('user-token')
            postOrderStatus.url = `/orders/${query.OrderId}/statuses`
            Object.assign(postOrderStatus.data, query)
            console.log('post order statuses', postOrderStatus)

            axios(postOrderStatus)
                .then(resp => {
                    console.log('post order status code_verify response', resp)
                    return !resp.data.Error
                        ? Promise.resolve(resp)
                        : Promise.reject(resp)
                })
                .then(resp => {
                    const data = resp.data
                    resolve(data)
                })
                .catch(resp => {
                    console.log('menu list err', resp)

                    reject(resp)
                })
        })
    },
    [SERVER_LAST_ORDERS]: ({commit, dispatch}) => {
        return new Promise((resolve, reject) => {

            const getLastOrders = server.getLastOrders
            getLastOrders.headers.authorization = 'Token ' + localStorage.getItem('user-token')

            axios(getLastOrders)
                .then(resp => {
                    console.log('SERVER_LAST_ORDERS response', resp)
                    return !resp.data.Error
                        ? Promise.resolve(resp)
                        : Promise.reject(resp)
                })
                .then(resp => {
                    const data = resp.data
                    resolve(data)
                })
                .catch(resp => {
                    console.log('SERVER_LAST_ORDERS err', resp)

                    reject(resp)
                })
        })
    },
    [SERVER_GET_DISCOUNT]: ({commit, dispatch}) => {
        return new Promise((resolve, reject) => {

            const getDiscount = server.getDiscount
            getDiscount.headers.authorization = 'Token ' + localStorage.getItem('user-token')

            axios(getDiscount)
                .then(resp => {
                    console.log('SERVER_GET_DISCOUNT response', resp)
                    return !resp.data.Error
                        ? Promise.resolve(resp)
                        : Promise.reject(resp)
                })
                .then(resp => {
                    const data = resp.data
                    commit(SERVER_GET_DISCOUNT, data)
                    resolve(data)
                })
                .catch(resp => {
                    console.log('SERVER_GET_DISCOUNT err', resp)

                    reject(resp)
                })
        })
    },
    [SET_BASKET]: ({commit, dispatch}, basket) => {
        commit(SET_BASKET, basket)
    },
}

const mutations = {
    [SERVER_CAFE_LIST]: (state, resp) => {
        state.cafeList = resp;
        state.cafeList.forEach(_ => _.isLoad = false)
        state.cafeList[0].Pic = ""
    },
    [SERVER_SEND_ORDER]: (state, lastOrder) => {
        state.lastOrder = lastOrder
    },
    [SERVER_GET_ORDER_BY_ID]: (state, resp) => {
        state.lastGetPayment = resp
    },
    [SERVER_GET_DISCOUNT]: (state, resp) => {
        state.discount = resp
    },
    [SET_BASKET]: (state, basket) => {
        console.log('state basket', basket)
        state.basket = basket
    }
}

export default {
    state,
    getters,
    actions,
    mutations
}

function onNotificationOpen() {
    window.FirebasePlugin.onNotificationOpen(function (notification) {
        console.log(notification)
    }, function (error) {
        console.error(error)
    })
}
