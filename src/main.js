import 'material-design-icons-iconfont/dist/material-design-icons.css'

import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import Vuetify from 'vuetify'
import 'vuetify/dist/vuetify.min.css'
import VueAuthenticate from 'vue-authenticate'

import Axios from 'axios'

const API_ENDPOINT = process.env.VUE_APP_BASE_URL
Axios.defaults.baseURL = API_ENDPOINT

Vue.use(Vuetify)


Vue.use(VueAuthenticate, {
  baseUrl: 'http://localhost:8080', // Your API domain

  providers: {
    github: {
      clientId: '',
      redirectUri: 'http://localhost:8080/login' // Your client app URL
    }
  }
})

Vue.config.productionTip = false

router.beforeEach((to, from, next) => {
    if (to.name === 'onboarding') {
        const entried = localStorage.getItem('entried')
        if (!entried) {
            next()
        } else {
            next('/places')
        }
    } else {
        next()
    }
})



new Vue({
    router,
    store,
    render: h => h(App)
}).$mount('#app')
